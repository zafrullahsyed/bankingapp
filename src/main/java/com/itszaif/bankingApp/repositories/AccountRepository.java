package com.itszaif.bankingApp.repositories;

import com.itszaif.bankingApp.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository to retrieve and delete {@link Account}.
 *
 * @author <a href="mailto:zafrullahmehdi@gmail.com">Zafrullah Syed</a>
 * @since 02.10.2018
 */
@Repository
public interface AccountRepository extends JpaRepository<Account, Integer>
{

    @Query("SELECT a FROM Account a WHERE a.accountNumber = :accountNumber")
    Account findByAccountNumber(@Param("accountNumber") int accountNumber);

    @Query("SELECT a FROM Account a WHERE a.accountNumber IN :accountNumbers AND a.userId = " +
           ":userId")
    List<Account> getBalanceForAccountsList(@Param("accountNumbers") List<Integer> accountNumbers,
                                            @Param("userId") int userId);

    @Query("SELECT a FROM Account a WHERE a.accountNumber = :accountNumber AND a.userId = :userId")
    Account findByAccountNumberAndUserId(@Param("accountNumber") int accountNumber,
                                         @Param("userId") int userId);
}
