package com.itszaif.bankingApp.repositories;

import com.itszaif.bankingApp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Repository to retrieve and delete {@link User}.
 *
 * @author <a href="mailto:zafrullahmehdi@gmail.com">Zafrullah Syed</a>
 * @since 02.10.2018
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer>
{
    @Query("select u from User u where u.userId = :userId")
    User findByUserId(@Param("userId") int userId);
}
