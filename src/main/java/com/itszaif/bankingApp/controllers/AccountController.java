package com.itszaif.bankingApp.controllers;

import com.itszaif.bankingApp.model.InputRequest.BalanceInputRequest;
import com.itszaif.bankingApp.model.InputRequest.TransferInputRequest;
import com.itszaif.bankingApp.model.InputRequest.WithdrawDepositInputRequest;
import com.itszaif.bankingApp.services.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * The Account Controller (RESTful service) is used to retrieve
 * {@link com.itszaif.bankingApp.model.Account Accounts}.
 *
 * @author <a href="mailto:zafrullahmehdi@gmail.com">Zafrullah Syed</a>
 * @since 02.10.2018
 */
@RestController
@RequestMapping("/account")
public class AccountController
{
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private AccountService accountService;

    /**
     * Get account balance of accounts
     *
     * @param inputRequest input request[userId]
     *
     * @return Map with accountNumber and account balance
     */
    @RequestMapping(value = "/balance", method = RequestMethod.POST)
    public ResponseEntity<Map<Integer, BigDecimal>> accountBalance(
            @Valid @NotEmpty @RequestBody BalanceInputRequest inputRequest)
    {
        Map<Integer, BigDecimal> accountBalanceMap = new HashMap<>();
        try
        {
            accountBalanceMap = accountService.checkAccountBalance(inputRequest);
            return new ResponseEntity<>(accountBalanceMap, HttpStatus.OK);
        }
        catch(Exception e)
        {
            logger.error("Exception occurred while checking balance for user: " +
                         inputRequest.getUserId());
            return new ResponseEntity<>(accountBalanceMap, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Withdraw amount from account.
     * @param inputRequest input request[userId, accountNumber, withdrawAmount]
     *
     * @return Map with accountNumber and account balance after withdraw
     */
    @RequestMapping(value = "/withdraw", method = RequestMethod.POST)
    public ResponseEntity<Map<Integer, BigDecimal>> amountWithdraw(
            @Valid @NotEmpty @RequestBody WithdrawDepositInputRequest inputRequest)
    {
        Map<Integer, BigDecimal> accountBalanceMap = new HashMap<>();

        try
        {
            accountBalanceMap = accountService.amountWithdraw(inputRequest);
            return new ResponseEntity<>(accountBalanceMap, HttpStatus.OK);
        }
        catch(Exception e)
        {
            logger.error("Exception occurred while withdraw for user: " + inputRequest.getUserId());
            return new ResponseEntity<>(accountBalanceMap, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Deposit amount into a account
     * @param inputRequest inputRequest[userId, accountNumber, withdrawAmount]
     *
     * @return Map with accountNumber and account balance after deposit
     */
    @RequestMapping(value = "/deposit", method = RequestMethod.POST)
    public ResponseEntity<Map<Integer, BigDecimal>> amountDeposit(
            @Valid @NotEmpty @RequestBody WithdrawDepositInputRequest inputRequest)
    {
        Map<Integer, BigDecimal> accountBalanceMap = new HashMap<>();
        try
        {
            accountBalanceMap = accountService.depositAmount(inputRequest);
            return new ResponseEntity<>(accountBalanceMap, HttpStatus.OK);
        }
        catch(Exception e)
        {
            logger.error("Exception occurred while deposit for user: " + inputRequest.getUserId());
            return new ResponseEntity<>(accountBalanceMap, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Transfer amount between accounts
     *
     * @param inputRequest input request[debitUserId, creditUserId, debitAccountNumber,
     * creditAccountNumber, transfer amount]
     *
     * @return map with accountNumber and remaining balance after transfer
     */
    @RequestMapping(value = "/transfer", method = RequestMethod.POST)
    public ResponseEntity<Map<Integer, BigDecimal>> amountTransfer(
            @Valid @NotEmpty @RequestBody TransferInputRequest inputRequest)
    {
        Map<Integer, BigDecimal> userAccountMap = new HashMap<>();

        try
        {
            userAccountMap = accountService.transferAmount(inputRequest);
            return new ResponseEntity<>(userAccountMap, HttpStatus.OK);
        }
        catch(Exception e)
        {
            logger.error(
                    "Exception occurred while transfer for user: " + inputRequest.getUserIdDebit());
            return new ResponseEntity<>(userAccountMap, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
