package com.itszaif.bankingApp.controllers;

import com.itszaif.bankingApp.model.Account;
import com.itszaif.bankingApp.model.InputRequest.CreateUserInputRequest;
import com.itszaif.bankingApp.model.InputRequest.UpdateUserInputRequest;
import com.itszaif.bankingApp.model.User;
import com.itszaif.bankingApp.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * The User Controller (RESTful service) is used to retrieve
 * {@link com.itszaif.bankingApp.model.User Users}.
 *
 * @author <a href="mailto:zafrullahmehdi@gmail.com">Zafrullah Syed</a>
 * @since 02.10.2018
 */
@RestController
@RequestMapping("/user")
public class UserController
{
    @Autowired
    private UserService userService;

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    /**
     * Create new user account
     * @param inputRequest input request
     *
     * @return new user with new account
     */
    @RequestMapping(value = "/createUser", method = RequestMethod.POST)
    public ResponseEntity<Account> createUserAccount(
            @NotEmpty @Valid @RequestBody CreateUserInputRequest inputRequest)
    {
        Account newUserAccount = null;
        try
        {
            newUserAccount = userService.createNewUserAccount(inputRequest);

            return new ResponseEntity<>(newUserAccount, HttpStatus.OK);
        }
        catch(Exception e)
        {
            logger.error("Exception while creating new user");
            return new ResponseEntity<>(newUserAccount, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update exisiting user
     *
     * @param inputRequest input request
     * @return updated user
     */
    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    public ResponseEntity<User> updateUserAccount(
            @NotEmpty @Valid @RequestBody UpdateUserInputRequest inputRequest)
    {
        User updatedUser = null;
        try
        {
            updatedUser = userService.updateExistingUser(inputRequest);
            return new ResponseEntity<>(updatedUser, HttpStatus.OK);
        }
        catch(Exception e)
        {
            logger.error("Exception while updating user");
            return new ResponseEntity<User>(updatedUser, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
