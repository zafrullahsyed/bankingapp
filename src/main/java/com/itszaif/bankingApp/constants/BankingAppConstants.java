package com.itszaif.bankingApp.constants;

import java.math.BigDecimal;

public interface BankingAppConstants
{
    /**
     * Savings account interest rate
     */
    BigDecimal SAVINGS_INTEREST_RATE = new BigDecimal(3.5);

    /**
     * Current account interest rate
     */
    BigDecimal CURRENT_INTEREST_RATE = new BigDecimal(5.2);

    /**
     * Over draft limit
     */
    BigDecimal OVER_DRAFT_LIMIT = new BigDecimal(-100);

    /**
     * Scale value
     */
    int SCALE_VALUE = 2;
}
