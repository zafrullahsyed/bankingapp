package com.itszaif.bankingApp.model.InputRequest;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Withdraw/Deposit Input request from frontend
 *
 * @author  <a href="mailto:zafrullahmehdi@gmail.com">Zafrullah Syed</a>
 * @since 02.10.2018
 */
public class WithdrawDepositInputRequest
{
    private int userId;
    private int accountNumber;
    private BigDecimal amount;

    /**
     * Gets account number.
     *
     * @return the account number
     */
    @NotNull
    public int getAccountNumber()
    {
        return accountNumber;
    }

    /**
     * Sets account number.
     *
     * @param accountNumber the account number
     */
    public void setAccountNumber(int accountNumber)
    {
        this.accountNumber = accountNumber;
    }

    /**
     * Gets user id.
     *
     * @return the user id
     */
    @NotNull
    public int getUserId()
    {
        return userId;
    }

    /**
     * Sets user id.
     *
     * @param userId the user id
     */
    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    /**
     * Gets amount.
     *
     * @return the amount
     */
    @NotNull
    public BigDecimal getAmount()
    {
        return amount;
    }

    /**
     * Sets amount.
     *
     * @param amount the amount
     */
    public void setAmount(BigDecimal amount)
    {
        this.amount = amount;
    }
}
