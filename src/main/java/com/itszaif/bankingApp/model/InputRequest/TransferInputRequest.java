package com.itszaif.bankingApp.model.InputRequest;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Transfer Input request from frontend
 *
 * @author  <a href="mailto:zafrullahmehdi@gmail.com">Zafrullah Syed</a>
 * @since 02.10.2018
 */
public class TransferInputRequest
{
    private int userIdCredit; // userId to deposit
    private int userIdDebit; // userId to withdraw
    private int accountNumberCredit; // Account to deposit
    private int accountNumberDebit; // Account to withdrawn
    private BigDecimal amount;

    /**
     * Gets user id credit.
     *
     * @return the user id credit
     */
    @NotNull
    public int getUserIdCredit()
    {
        return userIdCredit;
    }

    /**
     * Sets user id credit.
     *
     * @param userIdCredit the user id credit
     */
    public void setUserIdCredit(int userIdCredit)
    {
        this.userIdCredit = userIdCredit;
    }

    /**
     * Gets user id debit.
     *
     * @return the user id debit
     */
    @NotNull
    public int getUserIdDebit()
    {
        return userIdDebit;
    }

    /**
     * Sets user id debit.
     *
     * @param userIdDebit the user id debit
     */
    public void setUserIdDebit(int userIdDebit)
    {
        this.userIdDebit = userIdDebit;
    }

    /**
     * Gets account number credit.
     *
     * @return the account number credit
     */
    @NotNull
    public int getAccountNumberCredit()
    {
        return accountNumberCredit;
    }

    /**
     * Sets account number credit.
     *
     * @param accountNumberCredit the account number credit
     */
    public void setAccountNumberCredit(int accountNumberCredit)
    {
        this.accountNumberCredit = accountNumberCredit;
    }

    /**
     * Gets account number debit.
     *
     * @return the account number debit
     */
    @NotNull
    public int getAccountNumberDebit()
    {
        return accountNumberDebit;
    }

    /**
     * Sets account number debit.
     *
     * @param accountNumberDebit the account number debit
     */
    public void setAccountNumberDebit(int accountNumberDebit)
    {
        this.accountNumberDebit = accountNumberDebit;
    }

    /**
     * Gets amount.
     *
     * @return the amount
     */
    @NotNull
    public BigDecimal getAmount()
    {
        return amount;
    }

    /**
     * Sets amount.
     *
     * @param amount the amount
     */
    public void setAmount(BigDecimal amount)
    {
        this.amount = amount;
    }
}
