package com.itszaif.bankingApp.model.InputRequest;

import javax.validation.constraints.NotNull;

/**
 * Balance Input request from frontend
 *
 * @author  <a href="mailto:zafrullahmehdi@gmail.com">Zafrullah Syed</a>
 * @since 02.10.2018
 */
public class BalanceInputRequest
{
    private int userId;

    /**
     * Gets user id.
     *
     * @return the user id
     */
    @NotNull
    public int getUserId()
    {
        return userId;
    }

    /**
     * Sets user id.
     *
     * @param userId the user id
     */
    public void setUserId(int userId)
    {
        this.userId = userId;
    }
}
