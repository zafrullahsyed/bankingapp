package com.itszaif.bankingApp.model.InputRequest;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Update User Input request from frontend
 *
 * @author  <a href="mailto:zafrullahmehdi@gmail.com">Zafrullah Syed</a>
 * @since 02.10.2018
 */
public class UpdateUserInputRequest
{
    private int accountNumber;
    private String name;
    private int phoneNumber;
    private String accountType;

    /**
     * Gets account number.
     *
     * @return the account number
     */
    @NotNull
    public int getAccountNumber()
    {
        return accountNumber;
    }

    /**
     * Sets account number.
     *
     * @param accountNumber the account number
     */
    public void setAccountNumber(int accountNumber)
    {
        this.accountNumber = accountNumber;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    @NotBlank
    public String getName()
    {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Gets phone number.
     *
     * @return the phone number
     */
    @NotNull
    public int getPhoneNumber()
    {
        return phoneNumber;
    }

    /**
     * Sets phone number.
     *
     * @param phoneNumber the phone number
     */
    public void setPhoneNumber(int phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Gets account type.
     *
     * @return the account type
     */
    @NotBlank
    public String getAccountType()
    {
        return accountType;
    }

    /**
     * Sets account type.
     *
     * @param accountType the account type
     */
    public void setAccountType(String accountType)
    {
        this.accountType = accountType;
    }
}
