package com.itszaif.bankingApp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * Account Entity mapped to ACCOUNT database table
 *
 * @author  <a href="mailto:zafrullahmehdi@gmail.com">Zafrullah Syed</a>
 * @since 02.10.2018
 */
@Entity
@IdClass(Account.PK.class)
@Table(name = "ACCOUNT")
public class Account implements Serializable
{
    private static final long serialVersionUid = 513132137982321L;
    private int userId;
    private int accountNumber;
    private String accountType;
    private String accountOwner;
    private BigDecimal accountBalance;


    /**
     * Gets user id.
     *
     * @return the user id
     */
    @Id
    @Column(name = "USER_ID")
    public int getUserId()
    {
        return userId;
    }

    /**
     * Sets user id.
     *
     * @param userId the user id
     */
    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    /**
     * Gets account number.
     *
     * @return the account number
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
                    generator = "ACCOUNT_NUMBER_SEQ_GENERATOR")
    @SequenceGenerator(name = "ACCOUNT_NUMBER_SEQ_GENERATOR",
                       sequenceName = "ACCOUNT_NUMBER_SEQ",
                       allocationSize = 1)
    @Column(name = "ACCOUNT_NUMBER")
    public int getAccountNumber()
    {
        return accountNumber;
    }

    /**
     * Sets account number.
     *
     * @param accountNumber the account number
     */
    public void setAccountNumber(int accountNumber)
    {
        this.accountNumber = accountNumber;
    }

    /**
     * Gets account type.
     *
     * @return the account type
     */
    @Column(name = "ACCOUNT_TYPE", nullable = false)
    public String getAccountType()
    {
        return accountType;
    }

    /**
     * Sets account type.
     *
     * @param accountType the account type
     */
    public void setAccountType(String accountType)
    {
        this.accountType = accountType;
    }

    /**
     * Gets account owner.
     *
     * @return the account owner
     */
    @Column(name = "ACCOUNT_OWNER")
    public String getAccountOwner()
    {
        return accountOwner;
    }

    /**
     * Sets account owner.
     *
     * @param accountOwner the account owner
     */
    public void setAccountOwner(String accountOwner)
    {
        this.accountOwner = accountOwner;
    }

    /**
     * Gets account balance.
     *
     * @return the account balance
     */
    @Column(name = "BALANCE")
    public BigDecimal getAccountBalance()
    {
        return accountBalance;
    }

    /**
     * Sets account balance.
     *
     * @param accountBalance the account balance
     */
    public void setAccountBalance(BigDecimal accountBalance)
    {
        this.accountBalance = accountBalance;
    }

    @Override
    public boolean equals(Object o)
    {
        if(this == o)
        {
            return true;
        }
        if(o == null || getClass() != o.getClass())
        {
            return false;
        }
        Account account = (Account) o;
        return userId == account.userId &&
               accountNumber == account.accountNumber &&
               Objects.equals(accountType, account.accountType);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(userId, accountNumber, accountType);
    }

    /**
     * The type Pk.
     */
    public static class PK implements Serializable
    {
        private static final long serialVersionUID = 2312312L;
        private int userId;
        private int accountNumber;
        private String accountType;

        /**
         * Gets user id.
         *
         * @return the user id
         */
        public int getUserId()
        {
            return userId;
        }

        /**
         * Sets user id.
         *
         * @param userId the user id
         */
        public void setUserId(int userId)
        {
            this.userId = userId;
        }

        /**
         * Gets account number.
         *
         * @return the account number
         */
        public int getAccountNumber()
        {
            return accountNumber;
        }

        /**
         * Sets account number.
         *
         * @param accountNumber the account number
         */
        public void setAccountNumber(int accountNumber)
        {
            this.accountNumber = accountNumber;
        }

        /**
         * Gets account type.
         *
         * @return the account type
         */
        public String getAccountType()
        {
            return accountType;
        }

        /**
         * Sets account type.
         *
         * @param accountType the account type
         */
        public void setAccountType(String accountType)
        {
            this.accountType = accountType;
        }
    }
}
