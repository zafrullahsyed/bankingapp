package com.itszaif.bankingApp.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

/**
 * User Entity mapped to USER database table
 *
 * @author  <a href="mailto:zafrullahmehdi@gmail.com">Zafrullah Syed</a>
 * @since 02.10.2018
 */
@Entity
@Table(name = "USER")
public class User implements Serializable
{
    private static final long serialVersionUid = 5131321312132321L;

    private int userId;
    private String name;
    private int phoneNumber;
    private List<Account> userAccounts;

    /**
     * Gets user id.
     *
     * @return the user id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
                    generator = "USER_ID_SEQ_GENERATOR")
    @SequenceGenerator(name = "USER_ID_SEQ_GENERATOR",
                       sequenceName = "USER_ID_SEQ",
                       allocationSize = 1)
    @Column(name = "USER_ID")
    public int getUserId()
    {
        return userId;
    }

    /**
     * Sets user id.
     *
     * @param userId the user id
     */
    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    @Column(name = "NAME", nullable = false)
    public String getName()
    {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Gets phone number.
     *
     * @return the phone number
     */
    @Column(name = "PHONE", nullable = false)
    public int getPhoneNumber()
    {
        return phoneNumber;
    }

    /**
     * Sets phone number.
     *
     * @param phoneNumber the phone number
     */
    public void setPhoneNumber(int phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Gets user accounts.
     *
     * @return the user accounts
     */
    @OneToMany(mappedBy = "userId", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    public List<Account> getUserAccounts()
    {
        return userAccounts;
    }

    /**
     * Sets user accounts.
     *
     * @param userAccounts the user accounts
     */
    public void setUserAccounts(List<Account> userAccounts)
    {
        this.userAccounts = userAccounts;
    }
}
