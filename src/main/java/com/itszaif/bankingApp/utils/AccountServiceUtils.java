package com.itszaif.bankingApp.utils;

import com.itszaif.bankingApp.model.Account;

import java.util.ArrayList;
import java.util.List;

public class AccountServiceUtils
{
    public static List<Integer> extractAccountNumbers(List<Account> accountsList)
    {
        List<Integer> accountNumbersList = new ArrayList<>();
        if(accountsList != null && accountsList.size() > 0)
        {
            for(Account account : accountsList)
            {
                accountNumbersList.add(account.getAccountNumber());
            }
        }
        return accountNumbersList;
    }
}
