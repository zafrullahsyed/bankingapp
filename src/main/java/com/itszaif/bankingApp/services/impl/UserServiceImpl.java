package com.itszaif.bankingApp.services.impl;

import com.itszaif.bankingApp.model.Account;
import com.itszaif.bankingApp.model.InputRequest.CreateUserInputRequest;
import com.itszaif.bankingApp.model.InputRequest.UpdateUserInputRequest;
import com.itszaif.bankingApp.model.User;
import com.itszaif.bankingApp.repositories.AccountRepository;
import com.itszaif.bankingApp.repositories.UserRepository;
import com.itszaif.bankingApp.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * This class implements the {@link UserService} interface to update {@link User} Entities
 *
 * @author <a href="mailto:zafrullahmehdi@gmail.com">Zafrullah Syed</a>
 * @since 02.10.2018
 */
@Service
public class UserServiceImpl implements UserService
{
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public List<User> getUsers()
    {
        return userRepository.findAll();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.SERIALIZABLE,
                   rollbackFor = Exception.class)
    public Account createNewUserAccount(CreateUserInputRequest inputRequest) throws Exception
    {
        try
        {
            User user = new User();
            user.setName(inputRequest.getName());
            user.setPhoneNumber(inputRequest.getPhoneNumber());
            User newUser = userRepository.save(user);

            Account account = new Account();
            account.setUserId(newUser.getUserId());
            account.setAccountOwner(newUser.getName());
            account.setAccountType(inputRequest.getAccountType());
            account.setAccountBalance(BigDecimal.ZERO);

            return accountRepository.save(account);
        }
        catch(Exception e)
        {
            logger.error("Error while creating a new user account" + e);
            throw new Exception(e);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.SERIALIZABLE,
                   rollbackFor = Exception.class)
    public User updateExistingUser(UpdateUserInputRequest inputRequest) throws Exception
    {
        try
        {
            Account existingAccount =
                    accountRepository.findByAccountNumber(inputRequest.getAccountNumber());
            User existingUser = userRepository.findByUserId(existingAccount.getUserId());

            existingUser.setName(inputRequest.getName());
            existingUser.setPhoneNumber(inputRequest.getPhoneNumber());

            return userRepository.save(existingUser);
        }
        catch(Exception e)
        {
            logger.error("Error while updating user", e);
            throw new Exception(e);
        }
    }

    @Override
    public User findByUserId(int userId)
    {

        return userRepository.findByUserId(userId);
    }
}
