package com.itszaif.bankingApp.services.impl;

import com.itszaif.bankingApp.model.Account;
import com.itszaif.bankingApp.model.InputRequest.BalanceInputRequest;
import com.itszaif.bankingApp.model.InputRequest.TransferInputRequest;
import com.itszaif.bankingApp.model.InputRequest.WithdrawDepositInputRequest;
import com.itszaif.bankingApp.model.User;
import com.itszaif.bankingApp.repositories.AccountRepository;
import com.itszaif.bankingApp.repositories.UserRepository;
import com.itszaif.bankingApp.services.AccountService;
import com.itszaif.bankingApp.services.impl.AccountHelperUtility.AccountHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.itszaif.bankingApp.constants.BankingAppConstants.SCALE_VALUE;

/**
 * This class implements the {@link AccountService} interface to update {@link Account} Entities
 *
 * @author <a href="mailto:zafrullahmehdi@gmail.com">Zafrullah Syed</a>
 * @since 02.10.2018
 */
@Service
public class AccountServiceImpl implements AccountService
{
    private static final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AccountHelper accountHelper;

    /**
     *Check balance
     * @param inputRequest input request
     *
     * @return Map with accountNumber and account balance
     * @throws Exception exception
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.SERIALIZABLE,
                   rollbackFor = Exception.class)
    public Map<Integer, BigDecimal> checkAccountBalance(BalanceInputRequest inputRequest)
            throws Exception
    {
        User existingUser = userRepository.findByUserId(inputRequest.getUserId());
        List<Account> userAccounts = existingUser.getUserAccounts();

        try
        {
            return accountHelper.getAccountBalance(userAccounts);
        }
        catch(Exception e)
        {
            logger.error("Exception occurs while fetching account balance for userId: " +
                         existingUser.getUserId());
            throw new Exception(e);
        }
    }

    /**
     * Withdraw amount from account.
     * @param inputRequest input request
     *
     * @return Map with accountNumber and account balance after withdraw
     * @throws Exception exception
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.SERIALIZABLE,
                   rollbackFor = Exception.class)
    public Map<Integer, BigDecimal> amountWithdraw(WithdrawDepositInputRequest inputRequest)
            throws Exception
    {
        @NotNull int accountNumber = inputRequest.getAccountNumber();
        @NotNull BigDecimal withdrawAmount = inputRequest.getAmount();

        Map<Integer, BigDecimal> accountBalanceMap = new HashMap<>();

        Account account = accountRepository
                .findByAccountNumberAndUserId(accountNumber, inputRequest.getUserId());

        return accountHelper
                .getWithdrawMap(account, accountNumber, withdrawAmount, accountBalanceMap);
    }

    /**
     * Deposit amount into a account
     * @param request input request
     *
     * @return Map with accountNumber and account balance after deposit
     * @throws Exception exception
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.SERIALIZABLE,
                   rollbackFor = Exception.class)
    public Map<Integer, BigDecimal> depositAmount(WithdrawDepositInputRequest request)
            throws Exception
    {
        Map<Integer, BigDecimal> accountBalanceMap = new HashMap<>();
        @NotNull int accountNumber = request.getAccountNumber();

        try
        {
            Account account = accountRepository
                    .findByAccountNumberAndUserId(accountNumber, request.getUserId());
            BigDecimal accountBalance = account.getAccountBalance();

            BigDecimal balanceAfterDeposit = accountBalance.add(request.getAmount());
            account.setAccountBalance(balanceAfterDeposit);
            accountRepository.save(account);
            accountBalanceMap.put(accountNumber, balanceAfterDeposit.setScale(SCALE_VALUE, RoundingMode.HALF_UP));
        }
        catch(Exception e)
        {
            logger.error(
                    "Exception while deposit for account number " + accountNumber);
            throw new Exception();
        }
        return accountBalanceMap;
    }

    /**
     * Transfer amount between accounts
     *
     * @param inputRequest input request
     *
     * @return map with accountNumber and remaining balance after transfer
     * @throws Exception exception
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.SERIALIZABLE,
                   rollbackFor = Exception.class)
    public Map<Integer, BigDecimal> transferAmount(TransferInputRequest inputRequest)
            throws Exception
    {
        //debit -> withdrawFrom
        //credit -> depositInto
        Map<Integer, BigDecimal> accountBalanceMap = new HashMap<>();

        @NotNull int accountNumberDebit = inputRequest.getAccountNumberDebit();
        @NotNull int userIdDebit = inputRequest.getUserIdDebit();
        @NotNull int accountNumberCredit = inputRequest.getAccountNumberCredit();
        @NotNull int userIdCredit = inputRequest.getUserIdCredit();

        @NotNull BigDecimal transferAmountRequested = inputRequest.getAmount();
        try
        {
            Account debitAccountFrom =
                    accountRepository.findByAccountNumberAndUserId(accountNumberDebit, userIdDebit);

            Account creditAccountFrom = accountRepository
                    .findByAccountNumberAndUserId(accountNumberCredit, userIdCredit);

            accountBalanceMap = accountHelper
                    .getWithdrawMap(debitAccountFrom, accountNumberDebit, transferAmountRequested,
                                    accountBalanceMap);

            BigDecimal amountThatCanBeTransferred = accountBalanceMap.get(accountNumberDebit);

            if(amountThatCanBeTransferred.compareTo(BigDecimal.ZERO) > 0)
            {
                BigDecimal debitAccountBalance = debitAccountFrom.getAccountBalance();
                BigDecimal afterWithdrawDebitBalance =
                        debitAccountBalance.subtract(amountThatCanBeTransferred);
                debitAccountFrom.setAccountBalance(afterWithdrawDebitBalance);

                BigDecimal creditAccountBalance = creditAccountFrom.getAccountBalance();
                BigDecimal afterDepositDebitBalance =
                        creditAccountBalance.add(amountThatCanBeTransferred);
                creditAccountFrom.setAccountBalance(afterDepositDebitBalance);

                Account debitAccount = accountRepository.save(debitAccountFrom);
                Account creditAccount = accountRepository.save(creditAccountFrom);

                accountBalanceMap.put(userIdDebit, debitAccount.getAccountBalance());
                accountBalanceMap.put(userIdCredit, creditAccount.getAccountBalance());
            }
            else
            {
                // subtracted amount is greater than over draft limit
                logger.error("Insufficient funds");
                throw new Exception("Insufficient funds");
            }
        }
        catch(Exception e)
        {
            logger.error("Exception occurred while transfering amount from AccountNumber: " +
                         accountNumberDebit + "\n to Account number: " +
                         accountNumberCredit);
            throw new Exception();
        }
        return accountBalanceMap;
    }
}
