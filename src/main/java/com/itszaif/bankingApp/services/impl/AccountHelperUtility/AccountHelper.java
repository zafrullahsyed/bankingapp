package com.itszaif.bankingApp.services.impl.AccountHelperUtility;

import com.itszaif.bankingApp.model.Account;
import com.itszaif.bankingApp.repositories.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.itszaif.bankingApp.constants.BankingAppConstants.CURRENT_INTEREST_RATE;
import static com.itszaif.bankingApp.constants.BankingAppConstants.OVER_DRAFT_LIMIT;
import static com.itszaif.bankingApp.constants.BankingAppConstants.SAVINGS_INTEREST_RATE;
import static com.itszaif.bankingApp.constants.BankingAppConstants.SCALE_VALUE;

/**
 * This class is used by the AccountServiceImpl to perform calculations
 *
 * @author <a href="mailto:zafrullahmehdi@gmail.com">Zafrullah Syed</a>
 * @since 02.10.2018
 */
@Service
public class AccountHelper
{
    private static final Logger logger = LoggerFactory.getLogger(AccountHelper.class);

    @Autowired
    private AccountRepository accountRepository;

    /**
     * Check whether a user account has sufficient balance
     *
     * @param account account
     * @param accountNumber account Number
     * @param withdrawAmount withdraw amount
     * @param accountBalanceMap map with accountNumber and remaining balance
     *
     * @return map with accountNumber and remaining balance
     * @throws Exception exception
     */
    public Map<Integer, BigDecimal> getWithdrawMap(Account account, int accountNumber,
                                                   BigDecimal withdrawAmount,
                                                   Map<Integer, BigDecimal> accountBalanceMap)
            throws Exception
    {
        if(account.getAccountBalance().compareTo(withdrawAmount) > 0)
        {
            // existing amount is greater than required amount
            account.setAccountBalance(
                    account.getAccountBalance().subtract(withdrawAmount));
            accountRepository.save(account);
            accountBalanceMap.put(accountNumber, withdrawAmount);
        }
        else
        {
            if(account.getAccountType().equalsIgnoreCase("current"))
            {
                // Over draft only for current account, if saving then exception
                if(account.getAccountBalance().compareTo(BigDecimal.ZERO) > 0)
                {
                    // existing amount is greater than zero
                    BigDecimal subtractedAmount =
                            account.getAccountBalance().subtract(withdrawAmount);
                    if(subtractedAmount.compareTo(OVER_DRAFT_LIMIT) > 0)
                    {
                        // substracted amount is less than over draft limit
                        account.setAccountBalance(subtractedAmount);
                        accountRepository.save(account);
                        accountBalanceMap.put(accountNumber, withdrawAmount);
                    }
                    else
                    {
                        // subtracted amount is greater than over draft limit
                        logger.error("Insufficient funds");
                        throw new Exception("Insufficient funds");
                    }
                }
                else
                {
                    // existing amount is less than zero
                    BigDecimal amountThatCanBeGiven =
                            account.getAccountBalance().subtract(OVER_DRAFT_LIMIT);
                    if(amountThatCanBeGiven.compareTo(OVER_DRAFT_LIMIT) > 0)
                    {
                        //amount can be given less than requested
                        if(amountThatCanBeGiven.compareTo(withdrawAmount) > 0)
                        {
                            account.setAccountBalance(
                                    account.getAccountBalance().add(withdrawAmount.negate()));

                            accountRepository.save(account);
                            accountBalanceMap.put(accountNumber, amountThatCanBeGiven);
                        }
                        else
                        {
                            logger.error("insufficient funds");
                            throw new Exception("Insufficient funds");
                        }
                    }
                }
            }
            else
            {
                //savings
                logger.error("Insufficient funds");
                throw new Exception("Insufficient funds");
            }
        }
        return accountBalanceMap;
    }

    /**
     * Calculate account balance based on the account type and interest rate
     * @param userAccounts list of user accounts a user have
     *
     * @return map with accountNumbers and balance with calculated interest
     */
    public Map<Integer, BigDecimal> getAccountBalance(List<Account> userAccounts)
    {
        Map<Integer, BigDecimal> accountBalanceMap = new HashMap<>();
        for(Account account : userAccounts)
        {
            BigDecimal existingBalance = account.getAccountBalance();

            if(account.getAccountType().equalsIgnoreCase("savings"))
            {
                accountBalanceMap.put(account.getAccountNumber(),
                                      getBalance(existingBalance, SAVINGS_INTEREST_RATE));
            }
            else
            {
                accountBalanceMap.put(account.getAccountNumber(),
                                      getBalance(existingBalance, CURRENT_INTEREST_RATE));
            }
        }
        return accountBalanceMap;
    }

    private BigDecimal getBalance(BigDecimal existingBalance, BigDecimal interestRate)
    {
        return existingBalance.add(getInterest(existingBalance, interestRate));
    }

    private BigDecimal getInterest(BigDecimal existingBalance, BigDecimal interestRate)
    {
        return interestRate.multiply(existingBalance)
                           .divide(new BigDecimal(100), SCALE_VALUE, RoundingMode.HALF_UP)
                           .setScale(SCALE_VALUE, RoundingMode.HALF_UP);
    }
}
