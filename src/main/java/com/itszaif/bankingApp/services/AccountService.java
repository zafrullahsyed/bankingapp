package com.itszaif.bankingApp.services;

import com.itszaif.bankingApp.model.Account;
import com.itszaif.bankingApp.model.InputRequest.BalanceInputRequest;
import com.itszaif.bankingApp.model.InputRequest.TransferInputRequest;
import com.itszaif.bankingApp.model.InputRequest.WithdrawDepositInputRequest;

import java.math.BigDecimal;
import java.util.Map;

/**
 * This class is used by the AccountController to update {@link Account Account}.
 *
 * @author <a href="mailto:zafrullahmehdi@gmail.com">Zafrullah Syed</a>
 * @since 02.10.2018
 */
public interface AccountService
{
    Map<Integer, BigDecimal> checkAccountBalance(BalanceInputRequest inputRequest) throws Exception;

    Map<Integer, BigDecimal> amountWithdraw(WithdrawDepositInputRequest inputRequest)
            throws Exception;

    Map<Integer, BigDecimal> depositAmount(WithdrawDepositInputRequest request) throws Exception;

    Map<Integer, BigDecimal> transferAmount(TransferInputRequest inputRequest) throws Exception;
}
