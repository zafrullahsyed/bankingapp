package com.itszaif.bankingApp.services;

import com.itszaif.bankingApp.model.Account;
import com.itszaif.bankingApp.model.InputRequest.CreateUserInputRequest;
import com.itszaif.bankingApp.model.InputRequest.UpdateUserInputRequest;
import com.itszaif.bankingApp.model.User;

import java.util.List;

/**
 * This class is used by the UserController to update {@link User}.
 *
 * @author <a href="mailto:zafrullahmehdi@gmail.com">Zafrullah Syed</a>
 * @since 02.10.2018
 */
public interface UserService
{

    List<User> getUsers();

    Account createNewUserAccount(CreateUserInputRequest inputRequest) throws Exception;

    User updateExistingUser(UpdateUserInputRequest inputRequest) throws Exception;

    User findByUserId(int userId);
}
