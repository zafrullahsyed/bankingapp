package com.itszaif.bankingApp.repositories;

import com.itszaif.bankingApp.model.Account;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Account Repository test.
 *
 * @author <a href="mailto:zafrullahmehdi@gmail.com">Zafrullah Syed</a>
 * @since 02.10.2018
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountRepositoryTest
{

    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void test_all_existing_accounts()
    {
        List<Account> all = accountRepository.findAll();
        assertThat(all.size(), is(3));
    }
}