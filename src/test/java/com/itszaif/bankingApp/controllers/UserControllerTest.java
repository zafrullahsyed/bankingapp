package com.itszaif.bankingApp.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itszaif.bankingApp.model.InputRequest.CreateUserInputRequest;
import com.itszaif.bankingApp.model.InputRequest.UpdateUserInputRequest;
import com.itszaif.bankingApp.model.User;
import com.itszaif.bankingApp.repositories.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * User controller test.
 *
 * @author <a href="mailto:zafrullahmehdi@gmail.com">Zafrullah Syed</a>
 * @since 02.10.2018
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest
{

    private static ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void test_create_new_account_success() throws Exception
    {
        CreateUserInputRequest inputRequest = new CreateUserInputRequest();
        inputRequest.setName("Patrick Langer");
        inputRequest.setAccountType("savings");
        inputRequest.setPhoneNumber(135);

        ResultActions result = this.mockMvc.perform(
                post("/user/createUser/").contentType(MediaType.APPLICATION_JSON).content(
                        mapper.writer().writeValueAsString(inputRequest)));
        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$.userId", is(1)));
    }

    @Test()
    public void test_create_new_account_input_validation_fail() throws Exception
    {
        CreateUserInputRequest inputRequest = new CreateUserInputRequest();
        inputRequest.setName("Patrick Langer");

        ResultActions result = this.mockMvc.perform(
                post("/user/createUser/").contentType(MediaType.APPLICATION_JSON).content(
                        mapper.writer().writeValueAsString(inputRequest)));
        result.andExpect(status().isBadRequest());
    }

    //Updating user test
    @Test
    public void test_update_existing_user() throws Exception
    {
        //before
        User byUserId = userRepository.findByUserId(-1);
        assertThat(byUserId.getPhoneNumber(), is(123456));

        UpdateUserInputRequest inputRequest = new UpdateUserInputRequest();
        inputRequest.setAccountNumber(-2);
        inputRequest.setName("John");
        inputRequest.setAccountType("current");
        inputRequest.setPhoneNumber(135);

        ResultActions result = this.mockMvc.perform(
                post("/user/updateUser/").contentType(MediaType.APPLICATION_JSON).content(
                        mapper.writer().writeValueAsString(inputRequest)));
        result.andExpect(status().isOk());

        //After
        result.andExpect(jsonPath("$.phoneNumber", is(135)));
    }

    @Test()
    public void test_update_existing_user_input_validation_fail() throws Exception
    {
        UpdateUserInputRequest inputRequest = new UpdateUserInputRequest();
        inputRequest.setName("John");

        ResultActions result = this.mockMvc.perform(
                post("/user/updateUser/").contentType(MediaType.APPLICATION_JSON).content(
                        mapper.writer().writeValueAsString(inputRequest)));
        result.andExpect(status().isBadRequest());
    }
}