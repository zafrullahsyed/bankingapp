package com.itszaif.bankingApp.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itszaif.bankingApp.model.InputRequest.BalanceInputRequest;
import com.itszaif.bankingApp.model.InputRequest.TransferInputRequest;
import com.itszaif.bankingApp.model.InputRequest.WithdrawDepositInputRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Account controller test.
 *
 * @author <a href="mailto:zafrullahmehdi@gmail.com">Zafrullah Syed</a>
 * @since 02.10.2018
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AccountControllerTest
{
    private static ObjectMapper mapper = new ObjectMapper();
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void test_get_account_balance() throws Exception
    {
        BalanceInputRequest inputRequest = new BalanceInputRequest();
        inputRequest.setUserId(-1);

        ResultActions result = this.mockMvc
                .perform(post("/account/balance").contentType(MediaType.APPLICATION_JSON)
                                                 .content(mapper.writer().writeValueAsString(
                                                         inputRequest)));
        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$.-1", is(103.5)));
    }

    @Test
    public void test_amount_withdraw() throws Exception
    {
        WithdrawDepositInputRequest inputRequest = new WithdrawDepositInputRequest();
        inputRequest.setUserId(-1);
        inputRequest.setAccountNumber(-1);
        inputRequest.setAmount(new BigDecimal(50));

        ResultActions result = this.mockMvc
                .perform(post("/account/withdraw").contentType(MediaType.APPLICATION_JSON)
                                                  .content(mapper.writer().writeValueAsString(
                                                          inputRequest)));
        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$.-1", is(50)));


    }

    @Test
    public void test_account_deposit() throws Exception
    {
        WithdrawDepositInputRequest inputRequest = new WithdrawDepositInputRequest();
        inputRequest.setUserId(-1);
        inputRequest.setAccountNumber(-1);
        inputRequest.setAmount(new BigDecimal(200));

        ResultActions result = this.mockMvc
                .perform(post("/account/deposit").contentType(MediaType.APPLICATION_JSON)
                                                 .content(mapper.writer().writeValueAsString(
                                                         inputRequest)));
        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$.-1", is(300.00)));
    }

    @Test
    public void test_account_transfer() throws Exception
    {
        TransferInputRequest inputRequest = new TransferInputRequest();
        inputRequest.setUserIdDebit(-1);
        inputRequest.setUserIdCredit(123);
        inputRequest.setAccountNumberDebit(-2);
        inputRequest.setAccountNumberCredit(456);
        inputRequest.setAmount(new BigDecimal(200));

        ResultActions result = this.mockMvc
                .perform(post("/account/transfer").contentType(MediaType.APPLICATION_JSON)
                                                  .content(mapper.writer().writeValueAsString(
                                                          inputRequest)));
        result.andExpect(status().isOk());
    }
}