package com.itszaif.bankingApp.services.impl;

import com.itszaif.bankingApp.model.Account;
import com.itszaif.bankingApp.model.InputRequest.CreateUserInputRequest;
import com.itszaif.bankingApp.model.InputRequest.UpdateUserInputRequest;
import com.itszaif.bankingApp.model.User;
import com.itszaif.bankingApp.repositories.AccountRepository;
import com.itszaif.bankingApp.repositories.UserRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

/**
 * User Service Impl test.
 *
 * @author <a href="mailto:zafrullahmehdi@gmail.com">Zafrullah Syed</a>
 * @since 02.10.2018
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest
{
    @InjectMocks
    private UserServiceImpl userServiceImpl;

    @Mock
    private UserRepository userRepository;

    @Mock
    private AccountRepository accountRepository;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = RuntimeException.class)
    public void test_create_new_account_fail_exception() throws Exception
    {
        CreateUserInputRequest inputRequest = new CreateUserInputRequest();
        inputRequest.setName("Patrick Langer");
        inputRequest.setAccountType("savings");
        inputRequest.setPhoneNumber(135);

        User user = new User();
        user.setUserId(1);
        user.setName("Test");
        user.setPhoneNumber(123);
        when(userRepository.save(any(User.class))).thenReturn(user);

        Account account = new Account();
        account.setUserId(1);
        account.setAccountNumber(1);
        account.setAccountOwner("Test");
        account.setAccountType("savings");
        when(accountRepository.save(account)).thenThrow(RuntimeException.class);

        Account newUserAccount = userServiceImpl.createNewUserAccount(inputRequest);
        assertThat(newUserAccount.getUserId(), is(nullValue()));
    }

    @Test(expected = Exception.class)
    public void test_update_existing_account_fail_exception() throws Exception
    {
        Account account = new Account();
        account.setUserId(1);
        account.setAccountNumber(1);
        account.setAccountOwner("Test");
        account.setAccountType("savings");

        when(accountRepository.findByAccountNumber(anyInt())).thenReturn(account);
        when(userRepository.findByUserId(anyInt())).thenThrow(Exception.class);

        User user = userServiceImpl.updateExistingUser(any(UpdateUserInputRequest.class));
        assertThat(account.getAccountOwner(), is("Test"));
        assertThat(user.getName(), is(nullValue()));
    }
}