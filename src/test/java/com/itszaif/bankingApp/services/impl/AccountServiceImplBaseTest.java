package com.itszaif.bankingApp.services.impl;

import com.itszaif.bankingApp.model.InputRequest.BalanceInputRequest;
import com.itszaif.bankingApp.model.InputRequest.WithdrawDepositInputRequest;

import java.math.BigDecimal;

/**
 * Account service impl base test class.
 *
 * @author <a href="mailto:zafrullahmehdi@gmail.com">Zafrullah Syed</a>
 * @since 02.10.2018
 */
public class AccountServiceImplBaseTest
{

    protected WithdrawDepositInputRequest getDepositInputRequest()
    {
        WithdrawDepositInputRequest input = new WithdrawDepositInputRequest();
        input.setUserId(-1);
        input.setAccountNumber(-1);
        input.setAmount(new BigDecimal(100));
        return input;
    }

    protected BalanceInputRequest getBalanceInputRequest()
    {
        BalanceInputRequest balanceInputRequest = new BalanceInputRequest();
        balanceInputRequest.setUserId(-1);
        return balanceInputRequest;
    }

    protected WithdrawDepositInputRequest getWithdrawInputRequest(int userId, int accountNumber,
                                                                  int balance)
    {
        WithdrawDepositInputRequest input = new WithdrawDepositInputRequest();
        input.setUserId(userId);
        input.setAccountNumber(accountNumber);
        input.setAmount(new BigDecimal(balance));
        return input;
    }
}
