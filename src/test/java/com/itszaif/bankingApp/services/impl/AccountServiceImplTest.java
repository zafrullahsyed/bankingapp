package com.itszaif.bankingApp.services.impl;

import com.itszaif.bankingApp.model.Account;
import com.itszaif.bankingApp.repositories.AccountRepository;
import com.itszaif.bankingApp.services.AccountService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Account Service impl test class.
 *
 * @author <a href="mailto:zafrullahmehdi@gmail.com">Zafrullah Syed</a>
 * @since 02.10.2018
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountServiceImplTest extends AccountServiceImplBaseTest
{
    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountRepository accountRepository;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void test_check_account_balance() throws Exception
    {
        Account account = accountRepository.findByAccountNumber(-1);
        assertThat(account.getAccountBalance(), is(new BigDecimal("100")));
        Map<Integer, BigDecimal> accountBalance =
                accountService.checkAccountBalance(getBalanceInputRequest());
        assertThat(accountBalance.get(-1), is(new BigDecimal("29.00")));
        assertThat(accountBalance.get(-2), is(new BigDecimal("430.00")));
    }

    @Test
    public void test_deposit_account() throws Exception
    {
        //before
        Account account = accountRepository.findByAccountNumber(-1);
        assertThat(account.getAccountBalance(), is(new BigDecimal("100")));

        //action
        Map<Integer, BigDecimal> userAccountMap =
                accountService.depositAmount(getDepositInputRequest());

        //after
        assertThat(userAccountMap.get(-1), is(new BigDecimal("200")));
    }

    // Scenario1: Existing amount greater than withdraw amount
    // Existing amount:100, withdraw amount:50, balance 50, Account Type:savings
    @Test
    public void test_withdraw_amount_scenario_1() throws Exception
    {
        //before
        Account savingsAccount = accountRepository.findByAccountNumber(-1);
        assertThat(savingsAccount.getAccountBalance(), is(new BigDecimal("100")));

        //after
        Map<Integer, BigDecimal> accountBalanceMap =
                accountService.amountWithdraw(getWithdrawInputRequest(-1, -1, 50));
        assertThat(accountBalanceMap.get(-1), is(new BigDecimal("50")));
    }

    // Scenario2: Existing amount less than withdraw amount
    // Existing amount:100, withdraw amount:200, Account Type:savings, exception expected
    @Test(expected = Exception.class)
    public void test_withdraw_amount_scenario_2() throws Exception
    {
        //before
        Account savingsAccount = accountRepository.findByAccountNumber(-1);
        assertThat(savingsAccount.getAccountBalance(), is(new BigDecimal("100")));

        //after
        accountService.amountWithdraw(getWithdrawInputRequest(-1, -1, 200));
        expectedEx.expect(Exception.class);
        expectedEx.expectMessage("Insufficient funds");
    }


    // Scenario3: Existing amount greater than withdraw amount
    // Existing amount:1000, withdraw amount:1050, Account Type:current
    @Test()
    public void test_withdraw_amount_scenario_3() throws Exception
    {
        //before
        Account currentAccount = accountRepository.findByAccountNumber(-2);
        assertThat(currentAccount.getAccountBalance(), is(new BigDecimal("1000")));

        //after
        Map<Integer, BigDecimal> accountBalanceMap =
                accountService.amountWithdraw(getWithdrawInputRequest(-1, -2, 1050));
        assertThat(accountBalanceMap.get(-2), is(new BigDecimal(-50)));
    }

    // Scenario4: Existing amount greater than withdraw amount and Over draft
    // Existing amount:1000, withdraw amount:1200, Account Type:current, expected: exception
    @Test(expected = Exception.class)
    public void test_withdraw_amount_scenario_4() throws Exception
    {
        //before
        Account currentAccount = accountRepository.findByAccountNumber(-2);
        assertThat(currentAccount.getAccountBalance(), is(new BigDecimal("1000")));

        //after
        accountService.amountWithdraw(getWithdrawInputRequest(-1, -2, 1200));
        expectedEx.expect(Exception.class);
        expectedEx.expectMessage("Insufficient funds");
    }


    // Scenario5: Existing amount less than zero
    // Existing amount:-10, withdraw amount:50, Account Type:current
    @Test()
    public void test_withdraw_amount_scenario_5() throws Exception
    {
        //before
        Account currentAccount = accountRepository.findByAccountNumber(456);
        assertThat(currentAccount.getAccountBalance(), is(new BigDecimal("-10")));

        //after
        Map<Integer, BigDecimal> accountBalanceMap =
                accountService.amountWithdraw(getWithdrawInputRequest(123, 456, 50));
        assertThat(accountBalanceMap.get(456), is(new BigDecimal(-60)));
    }

    // Scenario6: Existing amount less than zero
    // Existing amount:-10, withdraw amount:500, Account Type:current
    @Test(expected = Exception.class)
    public void test_withdraw_amount_scenario_6() throws Exception
    {
        //before
        Account currentAccount = accountRepository.findByAccountNumber(456);
        assertThat(currentAccount.getAccountBalance(), is(new BigDecimal("-10")));

        //after
        Map<Integer, BigDecimal> accountBalanceMap =
                accountService.amountWithdraw(getWithdrawInputRequest(123, 456, 500));
        expectedEx.expect(Exception.class);
        expectedEx.expectMessage("Insufficient funds");
    }
}