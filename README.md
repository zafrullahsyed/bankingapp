# Banking Application

  **Banking application that supports:**
* Creating a new User and account.
* Update user details
* Checking balance of multiple accounts a user
* Deposit money
* Withdraw money
* Cash transfer between accounts

## Rest Endpoints:
All the rest end points consume Json.

Create a new User:
```
http://localhost:8080/user/createUser
{"name":"Patrick Langer","phoneNumber":135,"accountType":"savings"}
```
Update existing User details:
```
http://localhost:8080/user/updateUser
{"accountNumber":-2,"name":"John","phoneNumber":135,"accountType":"current"}
```
Account operations rest end points:
Check account balance of one/multiple accounts:
```
http://localhost:8080/account/balance
{"userId":-1}
```
Withdraw amount from an account:
```
http://localhost:8080/account/withdraw
{"userId":-1,"accountNumber":-1,"amount":50}
```
Deposit amount to an account
```
http://localhost:8080/account/deposit
{"userId":-1,"accountNumber":-1,"amount":200}
```
Transfer amount between accounts
```
http://localhost:8080/account/transfer
{"userIdCredit":123,"userIdDebit":-1,"accountNumberCredit":456,"accountNumberDebit":-2,"amount":200}
```

### Running the tests
1) Controller test cases:
```
com.itszaif.bankingApp.controllers;
```
1) Service test cases:
Services test cases contain various scenarios.
```
com.itszaif.bankingApp.services.impl;
```

### Running the application

1)To run the application, run the following file:

```
com.itszaif.bankingApp.BankingAppApplication
```
## Tech stack
* Spring boot, Spring JPA, Spring WEB, Liquibase, H2 database
## Further improvements

* When running all test cases on class level, the db is unable to teardown and some test cases are failing. If running individual test cases, all pass through. On method level, the DB should be up and tear down after the test method is finished. 
* More validations require to check the correctness of the accounts.
* Data model could be improved.. e.g., I considered userId as Integer, instead I could have taken it as String and give country code preview like (DE_123). 
* Instead of integration tests, more testing tools like mockito could be used more extensively to make sure that the code coverage is completely acheived.
* Write more test scenarios 
